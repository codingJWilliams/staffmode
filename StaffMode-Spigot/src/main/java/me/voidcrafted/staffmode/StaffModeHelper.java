package me.voidcrafted.staffmode;

import me.lucko.luckperms.api.Contexts;
import me.lucko.luckperms.api.LuckPermsApi;
import me.lucko.luckperms.api.NodeFactory;
import me.lucko.luckperms.api.User;
import org.bukkit.entity.Player;

import java.util.concurrent.CompletableFuture;

public class StaffModeHelper {
    public static boolean inStaffMode(LuckPermsApi api, Player player) {
        return Boolean.valueOf(
                api.getUser(player.getUniqueId())
                        .getCachedData()
                        .getMetaData(Contexts.global())
                        .getMeta()
                        .getOrDefault("StaffModePlugin:inStaffMode", "false")
        );
    }
    public static CompletableFuture setStaffMode(LuckPermsApi api, Player player, boolean mode) {
        User user = api.getUser(player.getUniqueId());
        user.setPermission(api.getNodeFactory().makeMetaNode("StaffModePlugin:inStaffMode", String.valueOf(mode)).build());
        return api.getUserManager().saveUser(user);
    }
}
