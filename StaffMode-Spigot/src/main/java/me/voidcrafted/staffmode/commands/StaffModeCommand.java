package me.voidcrafted.staffmode.commands;

import me.voidcrafted.staffmode.StaffModeHelper;
import me.voidcrafted.staffmode.StaffModePlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.awt.*;
import java.util.concurrent.CompletableFuture;

public class StaffModeCommand implements CommandExecutor {
    StaffModePlugin plugin;
    public StaffModeCommand(StaffModePlugin p) {
        plugin = p;
    }


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        try { Player player = (Player) sender; }
        catch (ClassCastException e) {
            sender.sendMessage("Cannot run as console.");
            return false;
        }

        Player player = (Player) sender;

        if (args.length == 0) {
            if (!player.hasPermission("staffmode.own")) {
                TextComponent message = new TextComponent("You do not have permission for this");
                message.setColor(ChatColor.RED);
                player.spigot().sendMessage(message);
                return true;
            }
            boolean currentMode = StaffModeHelper.inStaffMode(plugin.api, player);
            CompletableFuture<Void> cf = StaffModeHelper.setStaffMode(plugin.api, player, !currentMode);
            cf.whenComplete((Void v, Throwable t) -> {
                if (!currentMode) {
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("messages.enabledSelf")));
                } else {
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("messages.disabledSelf")));
                }
            });
        }

        return true;
    }
}
