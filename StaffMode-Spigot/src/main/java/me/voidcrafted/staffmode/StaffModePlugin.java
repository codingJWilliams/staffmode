package me.voidcrafted.staffmode;

import me.lucko.luckperms.api.LuckPermsApi;
import me.voidcrafted.staffmode.commands.StaffModeCommand;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class StaffModePlugin extends JavaPlugin {
    public LuckPermsApi api;

    @Override
    public void onEnable() {
        // Plugin enable
        RegisteredServiceProvider<LuckPermsApi> provider = Bukkit.getServicesManager().getRegistration(LuckPermsApi.class);
        if (provider == null) {
            this.getLogger().severe("Could not get LuckPerms api - is LuckPerms installed? [disabling]");
            return;
        }
        api = provider.getProvider();
        // API is not null now

        this.getCommand("staffmode").setExecutor(new StaffModeCommand(this));
        api.getContextManager().registerCalculator(new StaffModeContextCalculator(this));

        this.saveDefaultConfig();
    }
}
