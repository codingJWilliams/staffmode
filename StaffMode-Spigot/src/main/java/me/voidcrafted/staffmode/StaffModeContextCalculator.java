package me.voidcrafted.staffmode;

import me.lucko.luckperms.api.Contexts;
import me.lucko.luckperms.api.User;
import me.lucko.luckperms.api.caching.MetaData;
import me.lucko.luckperms.api.caching.PermissionData;
import me.lucko.luckperms.api.context.ContextCalculator;
import me.lucko.luckperms.api.context.ContextManager;
import me.lucko.luckperms.api.context.ImmutableContextSet;
import me.lucko.luckperms.api.context.MutableContextSet;
import org.bukkit.entity.Player;

public class StaffModeContextCalculator implements ContextCalculator<Player> {
    StaffModePlugin plugin;
    public StaffModeContextCalculator(StaffModePlugin p) {
        plugin = p;
    }

    @Override
    public MutableContextSet giveApplicableContext(Player subject, MutableContextSet accumulator) {
        accumulator.add("staffmode", String.valueOf(StaffModeHelper.inStaffMode(plugin.api, subject)));
        return accumulator;
    }
}
